uwuencode and uwudecode
===========================

Nowadays, [base64] is dominant binary-to-text encoding scheme. In
the past, one of frequently used schemes was [uuencode].

I've got idea to make uwuencode and its counterpart uwudecode
when processing uuencoded message in a chat which was sent to me
by a colleague. Few lines in bash and we have another encoding
scheme, that is even less efficient that all named above.


Example
-------

This uwuencoded block contains UTF-8 encoded text:

```
ow0 oWO UWu OWO oW0 Uwu OwO uwu owo UWU OWO UwU 0wo oWo OwO 0W0 Owo 0wU
UWo UWo OWO Uwu OwO UW0 uwU uWU 0WU UwU OwO 0WU UWU 0w0 uwu oWO uWU UWu
owo oWU owU uwu uwu UWU UWo 0wo 0w0 oWU OWo OWo owo UWU UWo OWO 0w0 oWo
OWo 0W0 uwu oWo 0wU oWU OWO Uwu OwO owU oW0 uwu oWO UWo Uwu oWo oWo UWo
oW0 uwu 0WU UWo uWu 0WU uWu uwU Owo 0wU oWU UwU owO u_u OwO UW0 uwu UW0
UWu 0wo 0w0 0Wo OwO 0W0 uwu UWU OWo owo Uwo Uwu owU uwU Uw0 0WU 0WU owo
owU 0W0 OWo Uw0 Owo 0wU oWO UWo owO Uwu OwO UWU uwU ow0 UWo UWo owO Uwu
0wU Uwu uwU oWO 0WU UWu 0wU 0Wo owo oWO oW0 uwu UWo UWo UWo oWo OwO 0wU
owo 0WU 0WU UwU UW0 oW0 owU owU uwU uWU 0WU UwU owo oWo oWo OWO uwU UWu
0wU oWU 0wo uWu 0wU 0w0 uwu 0wU oWO UwU u_u Uwu OwO 0WU oW0 UWo owo uWU
Uwo owU owU owo uwU uWu UW0 Uwo OWO Uwu OwO Uw0 Uw0 UWU UWo UWo 0wo oWU
owU owo uwu UW0 UW0 OWO uWU 0wo 0WU UuU
```


[base64]:    https://www.rfc-editor.org/rfc/rfc4648
[uuencode]:  https://www.tuhs.org/cgi-bin/utree.pl?file=4BSD/usr/man/cat1/uuencode.1c
